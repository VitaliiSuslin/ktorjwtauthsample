package com.plcoding

import com.plcoding.data.user.MongoUserDataSource
import com.plcoding.data.user.User
import com.plcoding.plugins.*
import com.plcoding.security.hashing.SHA256HashingService
import com.plcoding.security.token.JwtService
import com.plcoding.security.token.TokenConfig
import io.ktor.server.application.*
import kotlinx.coroutines.runBlocking
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {

    val dbName = "jwt-training"

    val mongoUser= System.getenv("MONGO_USER")
    val mongoPw = System.getenv("MONGO_PW")
    val dbLink = System.getenv("MONGO_DB")
    val db = KMongo.createClient(
        connectionString = "mongodb+srv://$mongoUser:$mongoPw@$dbLink/?retryWrites=true&w=majority"
    ).coroutine
        .getDatabase(dbName)

//    val db = KMongo.createClient().coroutine
//        .getDatabase(dbName)
    val userDataSource = MongoUserDataSource(db)

    runBlocking {
        userDataSource.insertUser(User("test", "test-password", "salt"))
    }

    val tokenService = JwtService()
    val tokenConfig = TokenConfig(
        issuer = environment.config.property("jwt.issuer").getString(),
        audience = environment.config.property("jwt.audience").getString(),
        expiresIn = 365L * 1000L *60L * 60L * 24L,
        secret = System.getenv("JWT_SECRET")
    )

    val hashingService = SHA256HashingService()

    configureSecurity(tokenConfig)


    configureSerialization()
    configureMonitoring()
    configureRouting(hashingService, userDataSource, tokenService, tokenConfig)
}