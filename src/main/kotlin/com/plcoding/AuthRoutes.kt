package com.plcoding

import com.plcoding.data.requests.AuthRequest
import com.plcoding.data.responses.AuthResponse
import com.plcoding.data.user.User
import com.plcoding.data.user.UserDataSource
import com.plcoding.security.hashing.HashingService
import com.plcoding.security.hashing.SaltedHash
import com.plcoding.security.token.TokenClaim
import com.plcoding.security.token.TokenConfig
import com.plcoding.security.token.TokenService
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.signUp(
    hashingService: HashingService,
    userDataSource: UserDataSource
) {
    post("signup") {
        val result = kotlin.runCatching {
            call.receive<AuthRequest>()
        }

        val requestBody = result.getOrNull()

        if (result.isFailure || requestBody == null) {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }

        val areFieldsBlank = requestBody.username.isBlank() or requestBody.password.isBlank()
        val isPwTooShort = requestBody.password.length < 8
        if (areFieldsBlank or isPwTooShort) {
            call.respond(HttpStatusCode.Conflict)
            return@post
        }

        val saltedHash = hashingService.generateSaltedHash(requestBody.password)
        val user = User(
            userName = requestBody.username,
            password =  saltedHash.hash,
            salt = saltedHash.salt
        )

        val wasAcknowledged = userDataSource.insertUser(user)
        if (wasAcknowledged.not()) {
            call.respond(HttpStatusCode.Conflict)
            return@post
        }

        call.respond(HttpStatusCode.OK)
    }
}

fun Route.signIn(
    hashingService: HashingService,
    userDataSource: UserDataSource,
    tokenService: TokenService,
    tokenConfig: TokenConfig
) {
    post("signin") {
        val result = kotlin.runCatching {
            call.receive<AuthRequest>()
        }

        val requestBody = result.getOrNull()

        if (result.isFailure || requestBody == null) {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }

        val user = userDataSource.getUserByName(requestBody.username)
        if (user == null) {
            call.respond(HttpStatusCode.Conflict, "Incorrect username!")
            return@post
        }

        val isValidPassword = hashingService.verify(
            value = requestBody.password,
            saltedHash = SaltedHash(
                hash = user.password,
                salt = user.salt
            )
        )
        if (isValidPassword.not()) {
            call.respond(HttpStatusCode.Conflict, "Incorrect password!")
            return@post
        }

        val token = tokenService.generate(
            config = tokenConfig,
            TokenClaim(
                name = "userId",
                value = user.id.toString()
            )
        )
        call.respond(HttpStatusCode.OK, message = AuthResponse(token))
    }
}

fun Route.authenticate() {
    authenticate {
        get("authenticate") {
            call.respond(HttpStatusCode.OK)
        }
    }
}

fun Route.getSecretInfo() {
    authenticate {
        get("secret") {
            val principal = call.principal<JWTPrincipal>()
            val userId = principal?.getClaim("userId", String::class)
            call.respond(HttpStatusCode.OK, "Your userId is $userId")
        }
    }
}